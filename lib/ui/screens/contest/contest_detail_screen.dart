import 'package:flutter/material.dart';
import 'package:mishpix/models/contest_list_model.dart';
import 'package:mishpix/styles.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mishpix/ui/screens/contest/tab_details.dart';
import 'package:mishpix/ui/screens/contest/tab_instructions.dart';
import 'package:mishpix/ui/screens/contest/contest_submit_photo_screen.dart';



class ContestDetailScreen extends StatefulWidget {

  final Category category;
  ContestDetailScreen({Key key,this.category}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ContestDetailState();

}

class _ContestDetailState extends State<ContestDetailScreen> with SingleTickerProviderStateMixin
{
  TabController _tabController;
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Details'),
    Tab(text: 'Instructions'),
  ];
  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
  }
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  List<String> _listViewImages= [
    "https://placeimg.com/640/480/any0",
    "https://placeimg.com/480/640/any1",
    "https://placeimg.com/800/600/any",
    "https://placeimg.com/480/640/any",
    "https://placeimg.com/640/480/any",
    "https://placeimg.com/1600/900/any",
    "https://placeimg.com/640/480/any",
    "https://placeimg.com/800/400/any",
    "https://placeimg.com/600/900/any",
    "https://placeimg.com/900/600/any",
    "https://placeimg.com/640/480/any0",
    "https://placeimg.com/480/640/any1",
    "https://placeimg.com/800/600/any",
    "https://placeimg.com/480/640/any",
    "https://placeimg.com/640/480/any",
    "https://placeimg.com/1600/900/any",
    "https://placeimg.com/640/480/any",
    "https://placeimg.com/800/400/any",
    "https://placeimg.com/600/900/any",
    "https://placeimg.com/900/600/any",

  ];


  @override
  Widget build(BuildContext context) {

    final levelIndicator = Container(
      child: Container(
        child: LinearProgressIndicator(
          backgroundColor: Color.fromRGBO(209, 224, 224, 0.2),
          value: 0.2,
          valueColor: AlwaysStoppedAnimation(Colors.green),
        ),
      ),
    );

    final contestPrize = Container(
      padding: EdgeInsets.all(7.0),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Text("\u20B9 "+widget.category.categoryPrize,style: Styles.constestTitleText,)
    );

    final topContentText = Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text(widget.category.categoryName,style: Styles.constestTitleText,),
        SizedBox(height: 30.0,),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(flex: 1,
            child: levelIndicator,
            ),
            Expanded(flex: 5,
            child: Padding(padding: EdgeInsets.only(left: 10.0),
            child: Text(widget.category.categoryLevel,style: Styles.constestSubTitleText,)
            ),
            ),
            Expanded(
              flex: 2,child: Center(
              child: contestPrize,
            ),
            ),

          ],
        )
      ],
    );

    final topContent = Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height*0.4,
          decoration: BoxDecoration(
           image: DecorationImage(
                image: NetworkImage(widget.category.imgUrl),
                fit: BoxFit.cover
            )
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.4,
          padding: EdgeInsets.all(40.0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Color.fromRGBO(58, 66, 86, 0.8),
          ),
          child: Center(
            child: topContentText,
          ),
        ),
      /*  Positioned(
          left: 20.0,
          top: 50.0,
          child: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back,color: Colors.white,size: 20.0,),
          ),
        )*/
      ],
    );


  final bottomContent =  StaggeredGridView.countBuilder(
      crossAxisCount: 4,
      itemCount: 20,
      itemBuilder: (BuildContext context,int index)=>Card(
        child: Column(
          children: <Widget>[
            Image.network(_listViewImages[index])
          ],
        ),
      ),
    staggeredTileBuilder: (int index)=> StaggeredTile.fit(2),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
  );

  final tabContent = TabBar(
    controller: _tabController,
    tabs: myTabs,
  );

  final tabBodyContent = TabBarView(
    controller: _tabController,
    children: [
      TabDetails(widget.category),
      TabInstructions()
    ]
  );



  return Scaffold(
    backgroundColor: Colors.white,
    body: NestedScrollView(
        headerSliverBuilder: (BuildContext context,bool innerBoxIsScrolled){
          return <Widget>[
                SliverAppBar(
                  iconTheme: IconThemeData(
                    color: Colors.white, //change your color here
                  ),
                  expandedHeight: MediaQuery.of(context).size.height*0.3,
                  floating: false,
                  pinned: false,
                  flexibleSpace: FlexibleSpaceBar(
                    background: topContent,
                  ),
                )
          ];
        },
        body: SizedBox(

          height: 300.0,
        child :Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(30.0),
            child: tabContent,
            ),

            Expanded(
              child: tabBodyContent,
            ),
            SizedBox(
              height: 50.0,
            )

          ],
        ))
    ),
    bottomSheet: SizedBox(
      width: double.infinity,
      child: RaisedButton(onPressed: (){
          goToContestSubmitPhoto(context);
      },
        color: Colors.blue,
      child: Padding(padding: EdgeInsets.all(15.0),
          child: Text("Submit Photo",style: Styles.constestTitleText,),
      )
      ),
    )
  );
  }

  void goToContestSubmitPhoto(BuildContext context)
  {
    var route = MaterialPageRoute(
        builder: (BuildContext context) => ContestSubmitPhotoScreen(category: widget.category,)
    );
    Navigator.push(context, route);
  }

}
