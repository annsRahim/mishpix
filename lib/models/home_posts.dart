// To parse this JSON data, do
//
//     final homePost = homePostFromJson(jsonString);

import 'dart:convert';

List<HomePost> homePostFromJson(String str) => new List<HomePost>.from(json.decode(str).map((x) => HomePost.fromJson(x)));

String homePostToJson(List<HomePost> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

  String allPostsToJson(List<HomePost> data) {
    final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
    return json.encode(dyn);
  }

    List<HomePost> allPostsFromJson(String str) {
      final jsonData = json.decode(str);
      return new List<HomePost>.from(jsonData.map((x) => HomePost.fromJson(x)));
    }


  HomePost postFromJson(String str) {
    final jsonData = json.decode(str);
    return HomePost.fromJson(jsonData);
  }

  String postToJson(HomePost data) {
    final dyn = data.toJson();
    return json.encode(dyn);
  }

class HomePost {
  int userId;
  int id;
  String title;
  String body;

  HomePost({
    this.userId,
    this.id,
    this.title,
    this.body,
  });

  factory HomePost.fromJson(Map<String, dynamic> json) => new HomePost(
    userId: json["userId"],
    id: json["id"],
    title: json["title"],
    body: json["body"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId,
    "id": id,
    "title": title,
    "body": body,
  };


}
