import 'package:flutter/material.dart';
import 'package:mishpix/models/home_posts.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HomePostLists extends StatelessWidget{

  final List<HomePost> homePosts;


  const HomePostLists({Key key, this.homePosts}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListView.builder(
        itemCount: homePosts.length,
        itemBuilder: (context,position){
          return ListTile(
            subtitle: imagePost(context, homePosts[position]),
          );
          return imagePost(context, homePosts[position]);

        },
      ),
    );

  }

  _onTapItem(BuildContext context, HomePost homePost) {
    print("Home Post CLicked ${homePost.title}");
  }

  Card imagePost(BuildContext context,HomePost post){
    return Card(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          postHeader(context,post),
          CachedNetworkImage(
            imageUrl: 'https://picsum.photos/id/884/400/400',
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
            placeholder: (context, url) => new CircularProgressIndicator(),
            errorWidget: (context, url, error) => new Icon(Icons.error),
          ),
          postFooter()
        ],
      ),

    );

  }


  Container postHeader(BuildContext context,HomePost post)
  {
    return Container(
      margin: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 10),
                child: CircleAvatar(backgroundImage: NetworkImage("https://source.unsplash.com/random"),minRadius: 20.0,),
              ),

              SizedBox(
                width: 200,
                child: Text(
                  post.title,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.w600
                  ),
                ),
              )
            ],
          ),

          IconButton(
            icon: Icon(Icons.more_horiz),
            onPressed: () {
              _showPopupMenu(context);
            },
          )
        ],
      ),
    );
  }
  Padding postFooter()
  {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: Row(
        children: <Widget>[
          Icon(Icons.comment),
          Padding(
            padding: EdgeInsets.only(left: 10.0),
            child: Icon(Icons.favorite_border),
          )
        ],
      ),
    );
  }

  _showPopupMenu(BuildContext context) async {
     showMenu(
      context: context,
      position: RelativeRect.fromLTRB(100, 100, 100, 100),
      items: [
        PopupMenuItem(
          child: Text("View"),
        ),
        PopupMenuItem(
          child: Text("Edit"),
        ),
        PopupMenuItem(
          child: Text("Delete"),
        ),
      ],
      elevation: 8.0,
    );
  }

}