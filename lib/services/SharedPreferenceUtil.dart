import 'package:shared_preferences/shared_preferences.dart';

addStringToSF(String valueName,String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(valueName, value);
}

addIntToSF(String valueName,int value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt(valueName, value);
}

addDoubleToSF(String valueName,double value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setDouble(valueName, value);
}
addBoolToSF(String valueName,bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(valueName, value);
}

getStringSF(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString(value);
}

getIntSF(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getInt(value) ?? 0;
}

getDoubleSF(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getDouble(value);
}

getBoolSF(String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getBool(value);
}

removeAllValueSF() async
{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
}