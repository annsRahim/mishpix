import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:mishpix/models/home_posts.dart';
import 'package:mishpix/models/contest_list_model.dart';
import 'dart:io';
import 'dart:convert';

String url =  'https://jsonplaceholder.typicode.com/posts';
String BASE_URL = 'https://rshqsgyub4.execute-api.ap-south-1.amazonaws.com/dev';

Future<List<HomePost>> getAllHomePosts() async{
  final response = await http.get(url);
  return allPostsFromJson(response.body);
}

Future<HomePost> getHomePost() async{
  final response = await http.get('$url/1');
  return postFromJson(response.body);
}

Future<List<Category>> getContestList() async{
  final response = await http.get('$BASE_URL/api/contest');
  return contestListModelFromJson(response.body).categories;
}

Future<bool> postContestEntry(var body)async{
  return await http
      .post(Uri.encodeFull('$BASE_URL/api/contest_entry'), body: body, headers: {"Accept":"application/json"})
      .then((http.Response response) {
         print(response.body);
    final int statusCode = response.statusCode;
    if (statusCode < 200 || statusCode > 400 || json == null) {
      return false;
      throw new Exception("Error while fetching data");
    }
    print(response.body);
    return true;
  });
}
