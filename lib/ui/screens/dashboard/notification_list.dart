import "package:flutter/material.dart";
import 'package:mishpix/styles.dart';


class NotificationListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotificationListState();
}

class _NotificationListState extends State<NotificationListScreen> {


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Inbox"),
      ),
      body: Container(
        child: Center(
          child: Text("No Notifications"),
        ),
      ),
    );

    }
}

