import 'package:flutter/material.dart';
import 'package:mishpix/models/contest_list_model.dart';
import 'package:mishpix/services/UtilServices.dart';
import 'package:mishpix/services/post_services.dart';
import 'package:mishpix/styles.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:convert';
import 'package:mishpix/ui/widgets/ColorLoader3.dart';
import 'package:mishpix/ui/screens/contest/contest_upload_sucess.dart';
import 'package:mishpix/ui/screens/dashboard/dashboard.dart';
import 'package:http/http.dart' as http;

class ContestSubmitPhotoScreen extends StatefulWidget
{
  final Category category;
  @override
  State<StatefulWidget> createState() => _ContestSubmitPhotoState();
  ContestSubmitPhotoScreen({Key key,this.category}) : super(key: key);
}
class _ContestSubmitPhotoState extends State<ContestSubmitPhotoScreen>
{
  final uploadDescriptionController = TextEditingController();
  File imageFile;
  String userID;
  String userDescription = "No short description";
  Category category;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    category = widget.category;

  }
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    uploadDescriptionController.dispose();
    super.dispose();
  }
  Future pickImageFromGallery(ImageSource source) async{

    var image = await ImagePicker.pickImage(source: source,maxHeight: 600,maxWidth: 600);

    setState(() {
      imageFile =  image;

    });
  }

 /* Widget showImage(){
    return FutureBuilder<File>(
      future: futImageFile,
      builder: (BuildContext context,AsyncSnapshot<File> snapshot){
        if(snapshot.connectionState == ConnectionState.done && snapshot.data!=null){
          return Image.file(snapshot.data,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height-200,
            fit: BoxFit.contain,
          );
        }
        else if(snapshot.error != null)
          {
            var strS = snapshot.error.toString();
            return  Text(
              strS,
              textAlign: TextAlign.center,
            );
          }
        else{
          return const Text("No Image Selected",
          textAlign: TextAlign.center,
          );
        }
      },
    );
  }*/

  Widget showImage2(){
    return Center(
      child: imageFile == null
          ? Text('No image selected.')
          : Image.file(imageFile),
    );
  }

//  void _onLoading() {
//    showDialog(
//      context: context,
//      barrierDismissible: false,
//      builder: (BuildContext context){
//        return Dialog(
//          child: new Row(
//            mainAxisSize: MainAxisSize.min,
//            children: [
//              new ColorLoader3(
//                radius: 20.0,
//                dotRadius: 5.0,
//              ),
//              new Text("Uploading..."),
//            ],
//          ),
//        );
//      },
//
//    );
//  }
  void goToContestUploadSuccess(BuildContext context)
  {

    var route = MaterialPageRoute(
        builder: (BuildContext context) => DashboardScreen(currentTab: 1,)
    );
    Navigator.push(context, route);
  }

  Future<String> convertImageToBas64() async
  {
    List<int> imageBytes = await imageFile.readAsBytesSync();
    String base64Image =  base64Encode(imageBytes);
    print("Print 2");
    return base64Image;
  }

  void _uploadImageToServer() async
  {
    UtilServices.onLoading(context, "Uploading Photo");
    userID = await UtilServices.getUserID();
    if(uploadDescriptionController.text != "")
      userDescription = uploadDescriptionController.text;
    print("USER : "+userID);
    print("Print 1");
    String ccc = await convertImageToBas64();
    String url = "https://rshqsgyub4.execute-api.ap-south-1.amazonaws.com/dev/category";
    var body =  {
                  'type': 'contest_entry',
                  'categoryId': category.id,
                  'contestImageDesc' : userDescription,
                  'userId':userID,
                  'image' : ccc

              };
    print(body);
    Future<bool> postData = postContestEntry(body);
    postData.then((state)=>{
      checkForResult(state)
    });

  }

  void checkForResult(bool state)
  {
    Navigator.pop(context); //pop dialog
    if(state)
    goToContestUploadSuccess(context);
    else
      {
        print("Failed");
      }

  }



  Future<void> showConfirmUploadDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Upload Photo'),
          content: TextField(
            controller: uploadDescriptionController,
            decoration: InputDecoration(
                hintStyle: TextStyle(fontSize: 15.0, color: Color.fromRGBO(116, 114, 114,0.5)),
                hintText: '(Optional)Say something about the photo'
            ),
          ),
          actions: <Widget>[

            OutlineButton(
              child: Text('Upload'),
              onPressed: () {
              //  Navigator.pop(context); //pop dialo

                    _uploadImageToServer();


              },
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
            ),

          ],
        );
      },
    );
  }



  @override
  Widget build(BuildContext context) {


    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              if(imageFile!=null)
                showConfirmUploadDialog(context);
              else
                {
                  UtilServices.showAlertDialog(context,"Photo Not Found", "Choose a image to continue");
                }
            },
            child: Text("Done",style: Styles.menuText,),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Flexible(
            flex: 9,
            child: Center(
              child: showImage2(),
            ),
          ),
          Flexible(
            flex: 1,
            child: SizedBox(
             width: double.infinity,
              child :RaisedButton(onPressed: (){
                pickImageFromGallery(ImageSource.gallery);
              },
              color: Colors.orange,
              child: Padding(padding: EdgeInsets.all(15.0),
                child: Text("Choose image from gallery",style: Styles.constestSubTitleText,),
              )
            ),

          ),)
        ],
      )
    );


  }

}