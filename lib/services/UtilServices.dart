import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mishpix/Constants.dart';
import 'package:mishpix/services/SharedPreferenceUtil.dart';
import 'package:mishpix/ui/widgets/ColorLoader3.dart';
class UtilServices{
  static void addFBDatasToSF(String token) async
  {
    final graphResponse = await http.get(
        'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture&access_token=${token}');
    final profile = json.decode(graphResponse.body);
    Map<String, dynamic> map = jsonDecode(graphResponse.body);
    addStringToSF(Constants.FB_NAME,map['name']);
    addStringToSF(Constants.FB_FIRST_NAME,map['first_name']);
    addStringToSF(Constants.FB_LAST_NAME,map['last_name']);
    addStringToSF(Constants.FB_EMAIL,map['email']);
    addStringToSF(Constants.FB_PICTURE,map['picture']['data']['url']);
    addStringToSF(Constants.FB_PROFILE_ID,map['id']);
    print(getStringSF((Constants.FB_NAME)));
    print(map['first_name']);
    print(map['last_name']);
    print(map['email']);
    print(map['picture']['data']['url']);
    print("Done");
  }
  static Future<String> getUserID() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user.uid;
    // here you write the codes to input the data into firestore
  }
  static void onLoading(BuildContext context,String message) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context){
        return Dialog(
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new ColorLoader3(
                radius: 20.0,
                dotRadius: 5.0,
              ),
              new Text(message),
            ],
          ),
        );
      },

    );
  }
  static void showAlertDialog(BuildContext context,title,message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}