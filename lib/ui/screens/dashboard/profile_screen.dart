import "package:flutter/material.dart";
import 'package:mishpix/Constants.dart';
import 'package:mishpix/services/SharedPreferenceUtil.dart';
import 'package:mishpix/styles.dart';
import 'package:mishpix/ui/screens/profile/MyAwardsScreen.dart';
import 'package:mishpix/ui/screens/profile/MyContestScreen.dart';
import 'package:mishpix/ui/screens/profile/MyFavourtiesScreen.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with SingleTickerProviderStateMixin {

  TabController _tabController;
  String userProfilePicture = "";
  String userName = "Loading...";
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'My Clicks',),
    Tab(text: 'Awards'),
    Tab(text: 'Favourites'),
  ];

  @override
  void initState() {
    super.initState();
    getProfilePicture();
    _tabController = TabController(vsync: this, length: myTabs.length);

  }
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void getProfilePicture() async{
     String uName = await getStringSF(Constants.FB_NAME);
     String uProfilePicture = await getStringSF(Constants.FB_PICTURE);
     setState(() {
       userName = uName;
       userProfilePicture = uProfilePicture;
     });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: buildProfileHeader(),
        ),
      /*Divider(
        color: Colors.black
        ,
      ),*/
        Expanded(
          flex: 4,
          child: buildProfileBody(),
        )
      ],
    ));
  }

  Widget buildProfileHeader() {
    return Container(
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 100.0,
                  height: 100.0,
                  margin: const EdgeInsets.only(left: 5.0, right: 5.0,top: 10.0),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: buildProfileImage(),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.all(Radius.circular(100.0)),
                      boxShadow: [
                        BoxShadow(blurRadius: 7.0, color: Colors.black)
                      ])),
              Text(userName,style: Styles.profileUserName,),
            //  Text("Bird Photographer loving the passion !!",style: Styles.profileUserDesc),

            ]
        )
    );
  }


  NetworkImage buildProfileImage() {
    print("User Profile Picture : "+userProfilePicture);
    return NetworkImage(
        userProfilePicture
    );
  }

  Widget buildProfileBody() {
    final tabContent = TabBar(
      labelColor: Colors.black,
      controller: _tabController,
      tabs: myTabs,
    );

    final tabBodyContent = TabBarView(
        controller: _tabController,
        children: [
          MyContestScreen(),
          MyAwardsScreen(),
          MyFavouritesScreen()
        ]
    );
    return SizedBox(
        height: 300.0,
        child :Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(10.0),
              child: tabContent,
            ),

            Expanded(
              child: tabBodyContent,
            ),
            SizedBox(
              height: 50.0,
            )

          ],
        ));
  }
}
/*
class getClipper extends CustomClipper<Path>
{
  @override
  Path getClip(Size size) {
    // TODO: implement getClip
    var path  = new Path();
    path.lineTo(0.0, size.height / 2);
    path.lineTo(size.width+size.height / 2, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {

    return true;
  }

}
*/
