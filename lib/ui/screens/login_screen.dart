import "package:flutter/material.dart";
import "package:firebase_auth/firebase_auth.dart";
import "package:flutter_facebook_login/flutter_facebook_login.dart";
import "package:flutter_auth_buttons/flutter_auth_buttons.dart";
import 'package:mishpix/services/UtilServices.dart';
import 'package:mishpix/ui/screens/dashboard/dashboard.dart';



class LoginScreen extends StatefulWidget{
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen>
{
  BuildContext mContext;
  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser myUser;
  bool isLogged = false;


  //Facebook Test User
  //test_xvwkcio_mishpix@tfbnw.net
  //mish@123

  Future<FirebaseUser> _loginWithFacebook() async {
    var facebookLogin = new FacebookLogin();
    var result = await facebookLogin.logIn(['email']);
    print(result);
    //var result = await facebookLogin.logInWithReadPermissions(['email']);
    debugPrint(result.errorMessage.toString());
    if(result.status == FacebookLoginStatus.loggedIn)
      {
        FacebookAccessToken myFbToken = result.accessToken;

        AuthCredential credential= FacebookAuthProvider.getCredential(accessToken: myFbToken.token);
        print(myFbToken.token);
        UtilServices.addFBDatasToSF(myFbToken.token);
        FirebaseUser user = await _auth.signInWithCredential(credential);
        return user;
      }
    return null;
  }

  void _loginWithFB()
  {
    _loginWithFacebook().then((response){
      if(response!=null)
        {
          myUser = response;
          isLogged = true;
          setState(() {});
          goToDashboardPage();
        }
    });
  }

  void goToDashboardPage()
  {
    Navigator.of(mContext).pushReplacement(MaterialPageRoute(
        builder: (mContext) => DashboardScreen()
    ));
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    mContext = context;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: isLogged ? null : FacebookSignInButton(
          onPressed: _loginWithFB,
          borderRadius: 5,
          text: "Login with facebook",
        ),
      ),
    );
  }

}

