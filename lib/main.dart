import 'package:flutter/material.dart';
import 'package:mishpix/services/AuthService.dart';
import 'package:mishpix/ui/screens/login_screen.dart';
import 'package:mishpix/ui/screens/contest/contest_screen.dart';
import 'package:mishpix/ui/screens/contest/contest_detail_screen.dart';
import 'package:mishpix/ui/screens/dashboard/dashboard.dart';
import 'package:mishpix/theme/style.dart';

AuthService appAuth = new AuthService();

void main() async
{
  Widget _defaultHome  = new  LoginScreen();
  bool _result = await appAuth.login();
  if(_result)
    {
      //_defaultHome = new DashboardScreen();
      _defaultHome = new DashboardScreen();
    }

      runApp(new MaterialApp(
        title: 'App',
        home: _defaultHome,
        theme: appTheme(),

        routes: <String,WidgetBuilder>{
          '/home' : (BuildContext context) => new DashboardScreen(),
          '/login' : (BuildContext context) => new LoginScreen(),
          '/contest' : (BuildContext context) => new ContestScreen(),
        },
      ));
}