// To parse this JSON data, do
//
//     final contestListModel = contestListModelFromJson(jsonString);

import 'dart:convert';

ContestListModel contestListModelFromJson(String str) => ContestListModel.fromJson(json.decode(str));

String contestListModelToJson(ContestListModel data) => json.encode(data.toJson());



class ContestListModel {
  bool status;
  List<Category> categories;

  ContestListModel({
    this.status,
    this.categories,
  });

  factory ContestListModel.fromJson(Map<String, dynamic> json) => ContestListModel(
    status: json["status"],
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
  };
}

class Category {
  String categoryName;
  Date endDate;
  int status;
  int createdAt;
  String imgUrl;
  String categoryPrize;
  String detailHeader;
  int updatedAt;
  String detailDescription;
  Date startDate;
  String categoryLevel;
  String id;
  String type;

  Category({
    this.categoryName,
    this.endDate,
    this.status,
    this.createdAt,
    this.imgUrl,
    this.categoryPrize,
    this.detailHeader,
    this.updatedAt,
    this.detailDescription,
    this.startDate,
    this.categoryLevel,
    this.id,
    this.type,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    categoryName: json["categoryName"],
    endDate: dateValues.map[json["endDate"]],
    status: json["status"],
    createdAt: json["createdAt"] == null ? null : json["createdAt"],
    imgUrl: json["imgUrl"],
    categoryPrize: json["categoryPrize"],
    detailHeader: json["detailHeader"],
    updatedAt: json["updatedAt"],
    detailDescription: json["detailDescription"],
    startDate: dateValues.map[json["startDate"]],
    categoryLevel: json["categoryLevel"],
    id: json["id"],
    type: json["type"],
  );

  Map<String, dynamic> toJson() => {
    "categoryName": categoryName,
    "endDate": dateValues.reverse[endDate],
    "status": status,
    "createdAt": createdAt == null ? null : createdAt,
    "imgUrl": imgUrl,
    "categoryPrize": categoryPrize,
    "detailHeader": detailHeaderValues.reverse[detailHeader],
    "updatedAt": updatedAt,
    "detailDescription": detailDescriptionValues.reverse[detailDescription],
    "startDate": dateValues.reverse[startDate],
    "categoryLevel": categoryLevelValues.reverse[categoryLevel],
    "id": id,
    "type": typeValues.reverse[type],
  };
}

enum CategoryLevel { BEGINNER, EXPERT }

final categoryLevelValues = EnumValues({
  "Beginner": CategoryLevel.BEGINNER,
  "Expert": CategoryLevel.EXPERT
});

enum DetailDescription { GHGHGHGHGHG_ED_FKSD_FSDF_DFKSDFN_SDKF_SKFSD_FDSNFKSDN_F }

final detailDescriptionValues = EnumValues({
  "GHGHGHGHGHG ED FKSD FSDF DFKSDFN SDKF SKFSD FDSNFKSDN F": DetailDescription.GHGHGHGHGHG_ED_FKSD_FSDF_DFKSDFN_SDKF_SKFSD_FDSNFKSDN_F
});

enum DetailHeader { DETAIL_TESTTTTT }

final detailHeaderValues = EnumValues({
  "DETAIL TESTTTTT": DetailHeader.DETAIL_TESTTTTT
});

enum Date { THE_28_OCT_2019, THE_27_NOV_2019, THE_20_OCT_2019 }

final dateValues = EnumValues({
  "20-Oct-2019": Date.THE_20_OCT_2019,
  "27-Nov-2019": Date.THE_27_NOV_2019,
  "28-Oct-2019": Date.THE_28_OCT_2019
});

enum Type { CATEGORY }

final typeValues = EnumValues({
  "category": Type.CATEGORY
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
