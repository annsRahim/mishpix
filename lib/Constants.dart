class Constants{
  static const String LogOut = 'Log Out';
  static const List<String> choices = [ LogOut ];
  static const FB_NAME = 'FB_NAME';
  static const FB_FIRST_NAME = 'FB_FIRST_NAME';
  static const FB_LAST_NAME = 'FB_LAST_NAME';
  static const FB_EMAIL = 'FB_EMAIL';
  static const FB_PICTURE = 'FB_PICTURE';
  static const FB_PROFILE_ID = 'FB_PROFILE_ID';
}
