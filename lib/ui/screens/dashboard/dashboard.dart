import "package:flutter/material.dart";
import 'package:mishpix/services/AuthService.dart';
import '../../../Constants.dart';
import 'profile_screen.dart';
import 'package:mishpix/theme/style.dart';
import 'home_screen.dart';
import 'user_search.dart';
import 'notification_list.dart';
import 'dart:async';
import 'package:mishpix/ui/widgets/fab_bottom_app_bar.dart';



class DashboardScreen extends StatefulWidget
{
   int currentTab = 0;

  DashboardScreen({this.currentTab});
  @override
  State<StatefulWidget> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> with SingleTickerProviderStateMixin
{

  final changeNotifier = new StreamController.broadcast();
  BuildContext mContext;

  TabController _tabController;
  final List<Tab> myTabs = <Tab>[
    new Tab(icon:  Icon(Icons.home)),
    new Tab(icon: Icon(Icons.search),),
    new Tab(icon:  Icon(Icons.person)),
    new Tab(icon:  Icon(Icons.person)),

  ];
  String _lastSelected = 'TAB: 0';





  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);

    _tabController.addListener(handleTabChnage);
    if(widget.currentTab!=null)
      {
        _tabController.index = widget.currentTab;
        handleTabChnage();
      }


  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    changeNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mContext = context;
    return new MaterialApp(
      color: Colors.white,
      home: DefaultTabController(length: 2, child: new Scaffold(
          appBar: AppBar(title: const Text('WisdomLeap'),
            actions: <Widget>[
              PopupMenuButton<String>(
                onSelected: choiceAction,
                itemBuilder: (BuildContext context){
                  return Constants.choices.map((String choice){
                    return PopupMenuItem<String>(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                },
              )
            ],
          ),

          floatingActionButtonLocation:
          FloatingActionButtonLocation.centerDocked,


          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.orange,
            child: const Icon(Icons.image), onPressed: () {
            Navigator.pushNamed(context, '/contest');
          },),
          body: TabBarView(
              controller: _tabController,
              children: [
            HomeScreen(),
           // UserSearchScreen(),
          //  NotificationListScreen(),
            ProfileScreen(),

          ]),
          backgroundColor: Colors.white,
//          bottomNavigationBar: customBottomAppBar()
          bottomNavigationBar: customFabBottomBar()
      )),
    );
  }


 FABBottomAppBar customFabBottomBar()
 {
   return FABBottomAppBar(
     centerItemText: 'Contest',
     color: Colors.black54,
     selectedColor: Colors.black87,
     notchedShape: CircularNotchedRectangle(),
     onTabSelected: _selectedTab,

     triggerTabChange: changeNotifier.stream,
     items: [
          FABBottomAppBarItem(iconData: Icons.home,text: 'Home'),
          //FABBottomAppBarItem(iconData: Icons.search,text: 'Search'),
         // FABBottomAppBarItem(iconData: Icons.notifications_none,text: 'Notifications'),
          FABBottomAppBarItem(iconData: Icons.person,text: 'Profile'),
     ],
   );
 }
  void _selectedTab(int index) {

    _tabController.index = index;

    setState(() {
      _lastSelected = 'TAB: $index';
    });

  }
  handleTabChnage() {
    print("Fresh Tab "+_tabController.index.toString());
    changeNotifier.sink.add(_tabController.index);
  }
  Widget customBottomAppBar()
  {
    return  BottomAppBar(
      color: Colors.orange,
      shape: CircularNotchedRectangle(),
      notchMargin: 6.0,
      child: new TabBar(
          controller: _tabController,
          tabs: myTabs)
    );
  }

  void choiceAction(String choice){
    if(choice == Constants.LogOut) {
      AuthService().logOut(mContext);
    }

  }

}



void goToProfilePage(BuildContext context)
{
  Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => ProfileScreen()
  ));
}




