import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mishpix/models/contest_list_model.dart';
import 'package:mishpix/services/UtilServices.dart';
import 'package:mishpix/styles.dart';
import 'package:mishpix/ui/screens/contest/contest_detail_screen.dart';
import 'package:mishpix/services/post_services.dart';

class ContestScreen extends StatefulWidget
{

  @override
  State<StatefulWidget> createState()  => _ContestScreenState();
}
class _ContestScreenState extends State<ContestScreen>
{

  List<Category> _contestList = List<Category>();
  String userID = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _populateContestList();
  }

  void _populateContestList() async{
     // UtilServices.onLoading(context,"Loading Contest...");
     Future<List<Category>> ft;
     ft = getContestList();
     ft.then((categoryLevelValues)=>{
       setState(() => {
        _contestList = categoryLevelValues
       })
     });

  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    RawMaterialButton _buildFavoriteButton(bool inFavorites) {
      return RawMaterialButton(
        constraints: const BoxConstraints(minWidth: 40.0, minHeight: 40.0),
        onPressed: (){},
        child: Icon(
          // Conditional expression:
          // show "favorite" icon or "favorite border" icon depending on widget.inFavorites:
          inFavorites == true ? Icons.favorite : Icons.favorite_border,

        ),
        elevation: 2.0,
        fillColor: Colors.white,
        shape: CircleBorder(),
      );
    }

    Padding _buildTitleSection(String title){
      return Padding(
        padding: EdgeInsets.all(15.0),
        child:  Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

              Text(
               title,
                style: Styles.constestTitleText,
              ),
            SizedBox(height: 10.0),
            Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Icon(Icons.timer,size: 20.0,color: Colors.white,),
                SizedBox(width: 5.0,),
                Text(
                    "Sample text",
                  style: Styles.constestSubTitleText,
                ),
              ],
            )
          ],
        ),
      );
    }

    ListTile _buildItemsForListView(BuildContext context, int index) {
      return ListTile(
        title: Text(_contestList[index].categoryName),
        subtitle: Text(_contestList[index].detailHeader.toString(), style: TextStyle(fontSize: 18)),
      );
    }

//    return Scaffold(
//        backgroundColor: Colors.white,
//        appBar: AppBar(
//          title: Text('Contest'),
//        ),
//        body: ListView.builder(
//          itemCount: _contestList.length,
//          itemBuilder: _buildItemsForListView,
//        )
//    );

//    return Container(
//      child: FutureBuilder<List<Category>>(
//          future: getContestList(),
//          builder: (context,snapshot){
//            print(snapshot.data);
//            if(snapshot.hasError) {
//              return Center(
//                child: Text("Network Error"),
//              );
//            }
//            else
//              {
//                return Text("Contestt");
//              }
//          }),
//    );

    Image showContestImage(int index)
    {
      if(_contestList[index].imgUrl !=null)
          return Image.network(
            _contestList[index].imgUrl,
            fit: BoxFit.cover,
          );
        else
          {
            return Image.asset('assets/images/no_image_available.jpg');
          }
    }

    Widget getContestList()
    {

     // print(_contestList[0]);
      if(_contestList.isEmpty)
        return Container(
          child: Center(
            child: Text("No Contest available right now"),
          ),
        );
      else
         return ListView.builder(
              itemCount: _contestList.length,
              itemBuilder: (BuildContext context,int index){
                return GestureDetector(
                  onTap: () {
                    print(_contestList[index]);
                    var route = MaterialPageRoute(
                        builder: (BuildContext context) => ContestDetailScreen(category: _contestList[index],)
                    );
                    Navigator.push(context, route);
                  },
                  child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0,vertical: 10.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            alignment: Alignment.bottomCenter,
                            children: <Widget>[
                              AspectRatio(
                                  aspectRatio: 16.0/9.0,
                                  child: showContestImage(index)
                              ),
                              Positioned(
                                child: _buildFavoriteButton(true),
                                top: 2.0,
                                right: 2.0,
                              ),
                              Container(
                                color: Colors.black.withOpacity(0.5),
                                child : _buildTitleSection(_contestList[index].categoryName),
                                // bottom : 2.0,

                              ),

                            ],
                          ),
                        ],

                      )
                  ),
                );
              });
    }

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Contest"),
        ),
        body: getContestList()
    );

  }






}