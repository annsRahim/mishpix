import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class MyFavouritesScreen extends StatelessWidget{
  List<String> _listViewImages= [
    "https://placeimg.com/640/480/any0",
    "https://placeimg.com/480/640/any1",
    "https://placeimg.com/800/600/any",
    "https://placeimg.com/480/640/any",
    "https://placeimg.com/640/480/any",
    "https://placeimg.com/1600/900/any",
  ];
  @override
  Widget build(BuildContext context) {
    final bottomContent =  StaggeredGridView.countBuilder(
      crossAxisCount: 4,
      itemCount: 6,
      itemBuilder: (BuildContext context,int index)=>Card(
        child: Column(
          children: <Widget>[
            Image.network(_listViewImages[index])
          ],
        ),
      ),
      staggeredTileBuilder: (int index)=> StaggeredTile.fit(2),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );

    return Container(
      child:bottomContent,
    );
  }

}