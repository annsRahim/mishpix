import 'package:flutter/material.dart';
import 'package:mishpix/models/contest_list_model.dart';
import 'package:mishpix/styles.dart';

class TabDetails extends StatelessWidget{

  final Category category;
  List<String> _detailHeader = [
    "Anns",
    "Michael Scofffield",
    "Fernard Sucre",
    "Bellisky",
    "Sara",
  ];
  List<String> _detailText = [
    "Lorem ipsum  dolor sit amet, ac leo nam, integer at, magna sit. Parturient sed. Pharetra nam, elit volutpat,et tempus. Odio pellentesque, tortor vel.\n Fermentum cursus nam. Et augue, ultricies erat. Placerat nunc. Porta facilisis, maecenas sed placerat, nulla est. Orci sit. Nulla neque, at pellentesque.Quis et, proin eget.",
    "Michael Scofffield",
    "Fernard Sucre",
    "Bellisky",
    "Sara",
  ];

  TabDetails(this.category);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Text(category.detailHeader),
        Text(category.detailDescription)
      ],
    );
    return Container(
        child: ListView.builder(
          itemCount: 5,
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 10.0,),
                Padding(
                  padding: EdgeInsets.only(left: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(category.detailHeader,style: Styles.tabDetailsHeader,),
                      SizedBox(height: 10.0,),
                      Text(_detailText[0],textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,maxLines: 10,style: Styles.tabDetailsText,),
                    ],
                  )
                ),
                SizedBox(height: 10.0,),
              ],
            )
          ),
        )
    );
  }

}