import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';

abstract class Styles {

  static const constestTitleText = TextStyle(
    color: Color.fromRGBO(255,255,255,0.8),
    fontFamily: 'NotoSans',
    fontSize: 20,
    fontStyle: FontStyle.normal,
    fontWeight: FontWeight.bold
  );
  static const constestSubTitleText = TextStyle(
      color: Color.fromRGBO(255,255,255,0.8),
      fontFamily: 'NotoSans',
      fontSize: 15,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold
  );
  static const tabDetailsHeader = TextStyle(
      color: Color.fromRGBO(0,0,0,0.8),
      fontFamily: 'NotoSans',
      fontSize: 18,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      decoration: TextDecoration.underline,

  );
  static const tabDetailsText = TextStyle(
    color: Color.fromRGBO(0,0,0,0.8),
    fontFamily: 'NotoSans',
    fontSize: 16,
    fontStyle: FontStyle.normal,
  );
  static const menuText = TextStyle(
    color: Color.fromRGBO(0,0,0,0.8),
    fontFamily: 'NotoSans',
    fontSize: 16,
    fontStyle: FontStyle.normal,
  );
  static const profileUserName = TextStyle(
    color: Color.fromRGBO(0,0,0,0.8),
    fontFamily: 'NotoSans',
    fontSize: 18,
    fontStyle: FontStyle.normal,
  );
  static const profileUserDesc= TextStyle(
    color: Color.fromRGBO(0,0,0,0.8),
    fontFamily: 'NotoSans',
    fontSize: 16,
    fontStyle: FontStyle.italic,
  );
}