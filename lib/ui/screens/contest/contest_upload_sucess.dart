import 'package:flutter/material.dart';
import 'package:mishpix/styles.dart';

class ContestUploadSucess extends StatelessWidget
{
  final int contestId;
  final Image image;

  ContestUploadSucess({
   @required this.contestId,
   @required this.image
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(

      ),
      body: Container(
        child: Center(
          child: Text("Upload Done"),
        ),
      ),
    );
  }



}