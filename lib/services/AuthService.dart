import 'dart:async';
import "package:flutter/material.dart";
import 'package:firebase_auth/firebase_auth.dart';
import 'package:mishpix/ui/screens/dashboard/profile_screen.dart';
import 'package:mishpix/ui/screens/login_screen.dart';

class AuthService{

  //Login
  Future<bool> login() async{
    var fbUser = await FirebaseAuth.instance.currentUser();
    print(fbUser);
    if(fbUser != null){
        return true;
      }
      else
        return false;
  }

  logOut(BuildContext context) async{
   await FirebaseAuth.instance.signOut();
   Navigator.of(context).pushReplacement(MaterialPageRoute(
       builder: (context) => LoginScreen()
   ));
  }


}