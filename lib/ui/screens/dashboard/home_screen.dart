import "package:flutter/material.dart";
import 'package:cached_network_image/cached_network_image.dart';
import 'package:mishpix/models/home_posts.dart';

import 'package:mishpix/services/post_services.dart';
import 'home_post_lists.dart';

class HomeScreen extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BodyLayout();
  }

}

class BodyLayout extends StatelessWidget {

  List<String> _listViewData = [
    "Anns",
    "Michael Scofffield",
    "Fernard Sucre",
    "Bellisky",
    "Sara",
  ];
  List<String> _listViewImages= [
    "https://placeimg.com/640/480/any0",
    "https://placeimg.com/480/640/any1",
    "https://placeimg.com/800/600/any",
    "https://placeimg.com/480/640/any",
    "https://placeimg.com/640/480/any",

  ];
  var screenWidth;
  var screenHeight;

  List<HomePost> homePostList;


  @override
  Widget build(BuildContext context) {

    screenWidth = MediaQuery.of(context).size.width;
    screenHeight = MediaQuery.of(context).size.height;



    Container postHeader(int index)
    {
      return Container(
        margin: EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: CircleAvatar(backgroundImage: NetworkImage("https://source.unsplash.com/random"),minRadius: 20.0,),
                ),
                Text(_listViewData[index])
              ],
            ),

            IconButton(
              icon: Icon(Icons.more_horiz),
              onPressed: () {

              },
            )
          ],
        ),
      );
    }

    Padding postFooter(int Index)
    {
      return Padding(
        padding: EdgeInsets.all(10.0),
        child: Row(
          children: <Widget>[
            Icon(Icons.comment),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Icon(Icons.favorite_border),
            )
          ],
        ),
      );
    }

    Card homePostCard(int index){
      return Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            postHeader(index),
            CachedNetworkImage(
              imageUrl: _listViewImages[index],
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
              placeholder: (context, url) => new CircularProgressIndicator(),
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
            postFooter(index)
          ],
        ),

      );

    }



    Card imagePost(int index){
      return Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            postHeader(index),
            CachedNetworkImage(
              imageUrl: _listViewImages[index],
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
              placeholder: (context, url) => new CircularProgressIndicator(),
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
            postFooter(index)
          ],
        ),

      );

    }

    
    return Container(
      child: FutureBuilder<List<HomePost>>(
          future: getAllHomePosts(),
          builder: (context,snapshot){
            if(snapshot.hasError) print(snapshot.error);

            return snapshot.hasData ? HomePostLists(homePosts: snapshot.data,) : Center(
              child: CircularProgressIndicator(),
            );
          }),
    );



    return
      Container(
          child : ListView.builder(
            itemCount: _listViewData.length,
            itemBuilder: (BuildContext ctxt,int index){
              return imagePost(index);
            },
          )
      );



  }


  Widget getPost(String name,String imgUrl) {
    return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(backgroundImage: NetworkImage("https://source.unsplash.com/random"),),
                      ),
                      Text(name)
                    ],
                  ),
                  IconButton(
                    icon: Icon(Icons.more_horiz),
                    onPressed: () {

                    },
                  )
                ],
              ),
            ),
            Container(
              constraints: BoxConstraints.expand(height: 1),
              color: Colors.grey,
            ),
            Container(

             child : Center(
               child: Image(image:
               NetworkImage(imgUrl)
               ),
                )
            ),

          ],
        )
    );
  }






  Widget getImageFooter(String name)
  {
    return ListTile(
      leading: CircleAvatar(
        radius: 30.0,
        backgroundImage: NetworkImage("https://source.unsplash.com/random"),),
      title: Text('Three-line ListTile'),
      subtitle: Text(
          'A sufficiently long subtitle warrants three lines.'
      ),
    );

  }
}

